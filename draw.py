#!/bin/env python3
import math
import raylib as rl
import kerbal

width = 1000
height = 1000
orbit_scale = 1
radius_scale = 1
# simulation time advances dT seconds per 1 real time second.
dT = 10000
T = 0
draw_orbit = True

def resize_window():
    global width, height, orbit_scale, radius_scale
    width = rl.GetScreenWidth()
    height = rl.GetScreenHeight()
    orbit_scale = (min(width, height)/2) / (kerbal.eeloo.orbit.a * 1.01)
#    radius_scale = orbit_scale
    radius_scale = 10 / kerbal.jool.radius
    for p in planets:
        p.radius = max(p.body_r * radius_scale, 3)

class Planet:
    def __init__(self, body: kerbal.Body, color: tuple):
        self.angle = body.orbit.mean_anomaly
        self.angular_v = body.orbit.avg_angular_velocity
        self.a = body.orbit.a
        self.body_r = body.radius

        self.color = color
        self.radius = max(body.radius * radius_scale, 3)

    def advance(self, delta):
        da = self.angular_v * delta
        self.angle = (self.angle + da) % (math.tau)

    def draw(self):
        x = int(math.cos(self.angle) * orbit_scale * self.a + width / 2)
        y = int(math.sin(self.angle) * orbit_scale * self.a + height / 2)
        rl.DrawCircle(x, y, self.radius, self.color)

    def draw_orbit(self):
        rl.DrawCircleLines(width // 2, height // 2, self.a * orbit_scale, rl.colors.DARKGRAY)

kerbin = Planet(kerbal.kerbin, rl.colors.BLUE)
planets = {
        Planet(kerbal.moho, rl.colors.BROWN),
        Planet(kerbal.eve, rl.colors.PURPLE),
        Planet(kerbal.kerbin, rl.colors.BLUE),
        Planet(kerbal.duna, rl.colors.RED),
        Planet(kerbal.dres, rl.colors.GRAY),
        Planet(kerbal.jool, rl.colors.GREEN),
        Planet(kerbal.eeloo, rl.colors.WHITE)
        }


if __name__ == '__main__':
    rl.SetTraceLogLevel(rl.LOG_ERROR)
    rl.InitWindow(width, height, b"KSP Orbits")
    rl.SetWindowState(rl.FLAG_WINDOW_RESIZABLE)
    rl.SetTargetFPS(60)
    resize_window()

    while not rl.WindowShouldClose():
        k = rl.GetKeyPressed()
        while k:
            match k:
                case rl.KEY_EQUAL | rl.KEY_KP_ADD:
                    dT += (0.1 * dT)
                case rl.KEY_MINUS | rl.KEY_KP_SUBTRACT:
                    dT = max(dT - (0.1 * dT), 0)
                case rl.KEY_SPACE:
                    draw_orbit = not draw_orbit
                case _:
                    pass
            k = rl.GetKeyPressed()


        if rl.IsWindowResized():
            resize_window()

        rl.BeginDrawing()
        rl.ClearBackground(rl.BLACK)
        rl.DrawCircle(width // 2,
                      height // 2,
    #                  kerbal.kerbol.radius * radius_scale,
                      min(width, height) // 100,
                      rl.YELLOW)

        advance = rl.GetFrameTime() * dT
        T += advance
        for p in planets:
            p.advance(advance)
            if draw_orbit:
                p.draw_orbit()
            p.draw()

        fps = rl.GetFPS()
        rl.DrawText(f"FPS: {fps}".encode(), 5, 5, 20, rl.colors.RAYWHITE)
        rl.DrawText("Speed: {:.1f}".format(dT).encode(), 5, 25, 20, rl.colors.RAYWHITE)
        rl.DrawText(f"Time: UT+ {int(T)}".encode(), 5, 45, 20, rl.colors.RAYWHITE)
        rl.DrawText(f"UT+ {kerbal.sec_to_ksp_days(int(T))}".encode(), 5, 65, 20, rl.colors.RAYWHITE)

        rl.EndDrawing()

    rl.CloseWindow()
