G = 6.67430E-11

import math
import datetime

class Body:
    def __init__(self, R, M, V, orbit, influence=float('inf'),  name=""):
        """ R: Radius in meters,
            M: Mass in kg,
            V: Sidereal rotational velocity in m/s
            orbit: Orbit of this body, if it is in orbit
            influence: Radius of sphere of influence in m from center of gravity, not surface"""
        self.radius = R
        self.mass = M
        self.sidereal_rotational_velocity = V
        self.orbit = orbit
        self.mu = G * M    # F = (G . M . m) / r^2
        self.surface_gravity = self.mu / self.radius**2
        self.escape_velocity = math.sqrt(2 * self.mu / self.radius)
        # https://en.wikipedia.org/wiki/Synchronous_orbit
        # Altitude of a stationary circular synchronous orbit
        self.sync_orbit = math.pow(self.mu * self.radius**2 / V**2, 1/3) - self.radius
        self.sphere_of_influence = influence
        self.name = name

    def __str__(self):
        r = f"Mass: {self.mass}, Radius: {self.radius}"
        if self.name:
            return f"{self.name}: {r}"
        else:
            return r

    def __repr__(self):
        return self.__str__()

    def orbit_velocity(self, h):
        """Speed of an orbit at altitude h in meters from the surface"""
        return math.sqrt(self.mu / (self.radius + h))

    def potential_energy(self, h):
        #TODO
        pass

    #this assumes you kill all velocity at apo and then kill all velocity again
    #when you reach the surface. wasteful
    #TODO: remove after to_land2 is complete
    def to_land(self, h):
        """The delta V needed to land from a circular equatorial prograde orbit"""
        return self.orbit_velocity(h) \
               + math.sqrt((2 * self.mu * h) / (self.radius * (self.radius + h)))\
               - self.sidereal_rotational_velocity

    def change_orbit(self, h1, h2):
        """The delta V needed to go from circular orbit of h1 to h2
            https://physics.info/orbital-mechanics-2/practice.shtml"""
        #TODO
        return "TODO"

        # E = K + U
        v12 = self.orbit_velocity(h1)**2
        v22 = self.orbit_velocity(h2)**2
        r1r2 = (self.radius + h1) * (self.radius + h2)
        dv = math.sqrt((v22 -  v12)+ (2 * self.mu / r1r2) * (h2 - h1))
        return dv

    #Assuming you lower the periapsis to touch the surface and
    #then kill all velocity once you reach the surface
    def to_land2(self, h):
        r = (h + self.radius) / 2
        #lowering periapsis:
        v1 = self.orbit(h) - self.orbit(r)
        pass

    def transfer_ideal_phase_angle(self, destination: "Body"):
        """Return the ideal phase angle in radians (anticlockwise).

        Both this body and the destination should be orbiting a single body.
        The returned angle is between the destination body and
        this body, anticlockwise, starting from the line between
        this body and the body that this body orbits.
        """
        # check if the two bodies are orbiting the same body
        if self.orbit.body is not destination.orbit.body:
            raise ValueError("The origin body and destination should orbit the same body")

        transfer_orbit = Orbit(self.orbit.body,
                               apoapsis=max(self.orbit.a, destination.orbit.a),
                               periapsis=min(self.orbit.a, destination.orbit.a),
                               from_center=True)
        # angle of the target body in radians from 12 o'clock,
        # clockwise when this body is in 6 o'clock:
        angle = (transfer_orbit.period_in_seconds / 2) * destination.orbit.avg_angular_velocity
        return math.pi - angle



class Orbit:
    def __init__(self, body: Body, apoapsis, periapsis, from_center: bool = False, mean_anomaly: float = math.pi):
        """Describe an orbit around a Body.

           body: The body being orbited around,
           apoapsis: Altitude at apoapsis from the surface or center, in meters,
           periapsis: Altitude at periapsis, in meters.
           from_center: Whether the apo and peri are from center of the body being
               orbited or surface (game reports from surface, ksp wiki gives from center.
           mean_anomaly: Mean anomaly at 0s UT.
        """

        self.body = body
        #attributes apo and peri are always from center
        if from_center:
            self.apoapsis = apoapsis
            self.periapsis = periapsis
        else:
            self.apoapsis = apoapsis + body.radius
            self.periapsis = periapsis + body.radius

        self.a = (self.apoapsis + self.periapsis)/2
        self._b2 = apoapsis * periapsis
        self.b = math.sqrt(self._b2)
        self.eccentricity = math.sqrt(1 - self._b2 / self.a ** 2)

        #Kepler's third law
        self.period_in_seconds = math.sqrt((4 * math.pi**2 * self.a**3) / (self.body.mu))
        self.period = sec_to_ksp_days(self.period_in_seconds)

        # angular velocity in radians/second
        self.avg_angular_velocity = (2 * math.pi) / self.period_in_seconds

        self.mean_anomaly = mean_anomaly

    def velocity(self, altitude):
        """ from vis-viva equation
            altitude: altitude from the surface in meters"""
        r = altitude + self.body.radius

        v2 = (self.body.mu) * ((2 / r) - (1 / self.a))
        return math.sqrt(v2)

    def __str__(self):
        return f"Orbit: orbiting {self.body.name}, with apo {self.apoapsis} m and pre {self.periapsis} m (from center of mass) or {self.apoapsis - self.body.radius} m, {self.periapsis - self.body.radius} m (from surface)"

    def __repr__(self):
        return self.__str__()


def ms_to_kmh(v):
    return 3.6 * v

def kmh_to_ms(v):
    return v / 3.6

def sec_to_ksp_days(s):
    left = s
    #days are 6 hours in ksp
    days = int(left // (3600 * 6))
    left = left % (3600 * 6)
    hours = int(left // (3600))
    left = left % 3600
    minutes = int(left // 60)
    seconds = int(left % 60)
    return f"{days}d, {hours}h, {minutes}m, {seconds}s"

def vertical_velocity_to_height(body: Body, velocity: float) -> float:
    """ Calculate the max height (in m) from surface on a body from
initial vertical velocity (in m/s) starting from the
surface (height 0) assuming no air drag. Also taking the into
account the fact that ↓gravitational force ~ ↑1/r^2

Work done by gravity = 1/2 . m . V1^2
Force of gravity at distance r is: F(r) = GmM/r^2 and
Work done by this force over a distance is W = ∫Fdr from r1 to r2
(r1 being starting distance between centers of the mass of each object,
and r2 being the same distance when the vertical velocity of the object
becomes zero)

So GmM∫dr/r^2 [over r1 to r2] = 1/2 . m . V1^2
So GM . [1/r1 - 1/r2] = (V1^2)/2 and μ = GM
So r2 = (2μr1) / (2μ - V1^2 . r1)"""

    μ = body.mu
    r1 = body.radius
    v1 = velocity

    r2 = (2 * μ * r1) / (2 * μ - v1**2 * r1)

    return r2 - r1

    # mgh = 1/2 . m . v^2 --> h = 1/2 . v^2 / g
    # assumes g is constant for now
    #g = body.mu / (body.radius ** 2)
    #h = (0.5 * velocity**2) / g
    #return (r2 - r1, h)
    #TODO: graph the difference between the integral and the approximation
    #the larger the velocity the larger the difference.

def kerbin_to_planet_transfer_ideal_phase_angle(planet: Body):
    """ Returns the ideal phase angle between the planet and kerbin,
anti clockwise, starting from the line from kerbin to kerbol"""
    o = Orbit(kerbol, kerbin.orbit.a, planet.orbit.a, True)
    # angle of the target planet in radians from 12 o'clock,
    # clockwise when kerbin is in 6 o'clock:
    angle = (o.period_in_seconds / 2) * planet.orbit.avg_angular_velocity
    return math.pi - angle





# if I put the mass from the wiki for kerbol, the computed sidereal orbital period of planets are not
# the same as the wiki. But if I put the reported Standard gravitational parameter / G then they are
# mostly correct to a few seconds.
kerbol = Body(2616E5, 1.1723328e+18/G, 3804.8, None, name="Kerbol")
#kerbol = Body(2616E5, 1.7565459e+28, 3804.8, None)
moho = Body(250E3, 2.5263314E21, 1.2982, Orbit(kerbol, 6315765981, 4210510628, True, mean_anomaly=3.14), influence=9646663, name='Moho')

eve = Body(7e5, 1.2243980E23, 54.636, Orbit(kerbol, 9931011387, 9734357701, True, mean_anomaly=3.14), influence=85109365, name="Eve")

kerbin = Body(6E5, 5.2915158E22, 174.94, Orbit(kerbol, 13599840256, 13599840256, True, mean_anomaly=3.14), influence=84159286, name="Kerbin")
mun = Body(2E5, 9.7599066E20, 9.0416, Orbit(kerbin, 12E6, 12E6, True), name="Mun")
minmus = Body(6E4, 2.6457580E19, 9.3315, Orbit(kerbin, 47E6, 47E6, True), name="Minmus")

duna = Body(320E3, 4.515E21, 30.688, Orbit(kerbol, 21783189163, 19669121365, True, mean_anomaly=3.14), influence=47921949, name="Duna")
ike = Body(13e4, 2.7821615e20, 12.467, Orbit(duna, 3296e3, 3104e3, True), name="Ike")
gilly = Body(13e3, 1.2420363e17, 2.8909, Orbit(eve, 48825e3, 14175e3, True), name="Gilly")

dres = Body(138E3, 3.2190937E20, 24.916, Orbit(kerbol, 46761053692, 34917642714, True, mean_anomaly=3.14), influence=32832840, name="Dres")

jool = Body(6E6, 4.2332127E24, 1047.2, Orbit(kerbol, 72212238387, 65334882253, True, mean_anomaly=0.1), influence=2.4559852E9, name="Jool")

eeloo = Body(21E4, 1.1149224E21, 67.804, Orbit(kerbol, 113549713200, 66687926800, True, mean_anomaly=3.14), influence=1.1908294E8, name="Eeloo")


agatrey1 = Body(1, 1, 1, Orbit(kerbol, 97_077_039_474, 20_097_624_591), name="Agatreyl")

planets = {moho, eve, kerbin, duna, dres, jool, eeloo}

# irl earth
earth = Body(6371e3, 5.97237e24, 464.581189, None, name="Earth")

if __name__ == '__main__':
    pass
